/**
 * Created by daisy on 16/3/26.
 */
//var fs=require("fs");
//
//var data=fs.readFileSync("input.txt");
//
//console.log(data.toString());
//console.log("程序执行结束");
////阻塞 一定要读完才可以执行下面的语句

var fs=require("fs");

fs.readFile('input.txt',function(err,data){//回调函数，读完之后执行,先执行其他的东西
    if(err) return console.log(err);
    console.log(data.toString());
})

console.log("程序执行结束");
//第一个实例在文件读取完后才执行完程序。 第二个实例我们呢不需要等待文件读取完，这样就可以在读取文件时同时执行接下来的代码，大大提高了程序的性能。
//因此，阻塞按是按顺序执行的，而非阻塞是不需要按顺序的，所以如果需要处理回调函数的参数，我们就需要写在回调函数内。