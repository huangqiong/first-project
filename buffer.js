/**
 * Created by daisy on 16/3/26.
 */
    //由于 在处理TCP流的时候一定要用二进制数据，所以buffer专门用来放二进制数据
var buf=new Buffer(20);//长度为10字节
var len=buf.write('www.baidu.com');//返回实际写入的大小
var buf1=new Buffer('菜鸟教程');
var buf2=new  Buffer('www.baidu.com');

//console.log(buf);
//console.log(len);
//console.log(buf1);
//
//console.log(buf2);

//从缓冲区读取数据
console.log(buf.toString('ascii',0,5));
console.log(buf.toString('utf8',0,8));
//转成json数据
console.log(buf.toJSON());
//一个byte字节有8个bite比特
//一个汉字有两个bite=16个字节
//所以1字节的 2的8次方的储存空间256 0-256

//缓冲区合并
console.log(Buffer.concat([buf1,buf2]).toString());


var buffer1=new Buffer('CBS');
var buffer2=new Buffer('ABCDEFGH');
//<0 buffer1在buffer2的前面
//=0 相等
//>0 在后面
console.log(buffer1.compare(buffer2));
buffer1.copy(buffer2);
console.log(buffer2);
console.log(buffer1);
//slice返回一个被截断出来的数组
console.log(buffer2.slice(3,6));
console.log(buffer2.length);