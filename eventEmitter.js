/**
 * Created by daisy on 16/3/26.
 */
//引入event模块，这个模块就提供了一个对象events.EventEmitter  他的功能就是事件的触发和监听
//var events= require('events');
//
//var  eventEmitter=new events.EventEmitter();
//
//eventEmitter.on('some_event',function(){
//    console.log('some_event事件触发');
//})
//
//
////每个事件都支持若干个时间监听器
//eventEmitter.on('some_event',function(){
//    console.log("第二次触发");
//})
//
////on 用来绑定函数，而emit用来触发函数
//setTimeout(function(){
//    eventEmitter.emit('some_event');
//},1000);
//
//console.log(eventEmitter.listeners('some_event').length);//返回监听这个数组的事件
//console.log(require('events').EventEmitter.listenerCount(eventEmitter,'some_event'));
//var events=require('events');
//var eventEmitter=new events.EventEmitter();

//var eventEmitter=new require('events').EventEmitter();
var events= require('events');

var  eventEmitter=new events.EventEmitter();
var listerner1 =function(){
    console.log('第一个监听器');
};

var listerner2=function(){
    console.log('第二个监听器');
}
eventEmitter.on('connection',listerner1);
eventEmitter.on('connection',listerner2);

console.log(require('events').EventEmitter.listenerCount(eventEmitter,'connection'));
eventEmitter.emit('connection');

eventEmitter.removeListener('connection',listerner1);
console.log(require('events').EventEmitter.listenerCount(eventEmitter,'connection'));
eventEmitter.emit('connection');